<?php

namespace Tests;

abstract class AbstractDatabase extends \PHPUnit_Extensions_Database_TestCase
{
    protected $dataSet;
    protected $dbConnection;
    /** @var \PDO $db */
    protected $db;

    /**
     * @return \PHPUnit_Extensions_Database_Operation_Composite
     */
    protected function getSetUpOperation() {
        return new \PHPUnit_Extensions_Database_Operation_Composite(array(
            \PHPUnit_Extensions_Database_Operation_Factory::DELETE_ALL(),
            \PHPUnit_Extensions_Database_Operation_Factory::INSERT()
        ));
    }

    /**
     * @return \PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    public function getConnection()
    {
        if (empty($this->dbConnection)) {
            $this->db->exec("
            CREATE TABLE `forklift_usage` (
              `id` INT NOT NULL,
              `forklift_id` VARCHAR(8) NOT NULL,
              `driver_id` VARCHAR(14) NULL,
              `start` DATETIME NOT NULL,
              `end` DATETIME,
              PRIMARY KEY (`id`)
            );");

            $this->dbConnection = $this->createDefaultDBConnection($this->db, 'devisen_test');
        }

        return $this->dbConnection;
    }

    /**
     * @return \PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    public function getDataSet()
    {
        return new ArrayDataSet(include(__DIR__ . '/Functional/data/' . $this->dataSet . '_before.php'));
    }

    /**
     * @return \PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    public function getResultDataSet()
    {
        return new ArrayDataSet(include(__DIR__ . '/Functional/data/' . $this->dataSet . '_after.php'));
    }

    /**
     * @param mixed $dataSet
     */
    public function setDataSet($dataSet)
    {
        $this->dataSet = $dataSet;
    }

    /**
     * @param string $table
     */
    protected function assertTable($table)
    {
        $actualDb = new \PHPUnit_Extensions_Database_DataSet_QueryDataSet($this->getConnection());
        $actualDb->addTable(
            $table,
            'SELECT * FROM ' . $table
        );

        $expectedDb = $this->getResultDataSet();

        $this->assertTablesEqual($expectedDb->getTable($table), $actualDb->getTable($table));
    }

    public function tearDown()
    {
        $this->db->exec('DROP TABLE IF EXISTS `forklift_usage`;');

        parent::tearDown();
    }
}

