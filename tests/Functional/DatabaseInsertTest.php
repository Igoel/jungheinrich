<?php

namespace Tests\Functional;

use Jungheinrich\Database;
use Tests\AbstractDatabase;

class DatabaseInsertTest extends AbstractDatabase
{
    protected $db;
    /** @var  Database */
    private $database;

    public function setUp()
    {
        $this->setDataSet('insert');
        $this->database = new Database();
        $this->db = $this->database->db;

        parent::setUp();
    }

    public function testInsert()
    {
        $this->database->save([
            'id' => 1,
            'forkliftId' => 'hapodpgw',
            'driverId' => 'isdgvg233fn2u32',
            'start' => '2016-04-01 00:00:00',
            'end' => null
        ]);

        $this->assertTable('forklift_usage');
    }
}

