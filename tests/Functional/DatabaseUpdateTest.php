<?php

namespace Tests\Functional;

use Jungheinrich\Database;
use Tests\AbstractDatabase;

class DatabaseUpdateTest extends AbstractDatabase
{
    protected $db;
    /** @var  Database */
    private $database;

    public function setUp()
    {
        $this->setDataSet('update');
        $this->database = new Database();
        $this->db = $this->database->db;

        parent::setUp();
    }

    public function testUpdate()
    {
        $this->database->update([
            'id' => 1,
            'end' => '2016-04-01 05:00:00'
        ]);

        $this->assertTable('forklift_usage');
    }
}

