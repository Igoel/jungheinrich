<?php

namespace Tests\Unit;

use Jungheinrich\Simulator;

class SimulatorTest extends \PHPUnit_Framework_TestCase
{
    public function testConstuctWithSimulateTableCreation()
    {
        $dataGeneratorMock = $this->getMock('Jungheinrich\DataGenerator');
        $dataBaseMock = $this->getMock('Jungheinrich\Database', ['createTable']);

        $dataBaseMock->expects($this->once())
            ->method('createTable');

        $simulator = new Simulator($dataGeneratorMock, $dataBaseMock, true);

        $this->assertInstanceOf('Jungheinrich\Simulator', $simulator);

        $reflProperty = new \ReflectionProperty($simulator, 'dataGenerator');
        $reflProperty->setAccessible(true);
        $actualDataGeneratorValue = $reflProperty->getValue($simulator);

        $reflProperty = new \ReflectionProperty($simulator, 'database');
        $reflProperty->setAccessible(true);
        $actualDatabaseValue = $reflProperty->getValue($simulator);

        $this->assertInstanceOf('Jungheinrich\DataGenerator', $actualDataGeneratorValue);
        $this->assertInstanceOf('Jungheinrich\Database', $actualDatabaseValue);
    }

    public function testConstuctWithoutSimulateTableCreation()
    {
        $dataGeneratorMock = $this->getMock('Jungheinrich\DataGenerator');
        $dataBaseMock = $this->getMock('Jungheinrich\Database', ['createTable']);

        $dataBaseMock->expects($this->never())
            ->method('createTable');

        $simulator = new Simulator($dataGeneratorMock, $dataBaseMock);

        $this->assertInstanceOf('Jungheinrich\Simulator', $simulator);

        $reflProperty = new \ReflectionProperty($simulator, 'dataGenerator');
        $reflProperty->setAccessible(true);
        $actualDataGeneratorValue = $reflProperty->getValue($simulator);

        $reflProperty = new \ReflectionProperty($simulator, 'database');
        $reflProperty->setAccessible(true);
        $actualDatabaseValue = $reflProperty->getValue($simulator);

        $this->assertInstanceOf('Jungheinrich\DataGenerator', $actualDataGeneratorValue);
        $this->assertInstanceOf('Jungheinrich\Database', $actualDatabaseValue);
    }

    public function testSimulateInsert()
    {
        $dataGeneratorMock = $this->getMock('Jungheinrich\DataGenerator', ['getSimulatedData']);
        $dataBaseMock = $this->getMock('Jungheinrich\Database', ['save']);

        $dataGeneratorMock->expects($this->once())
            ->method('getSimulatedData')
            ->will($this->returnValue(['end' => null]));

        $dataBaseMock->expects($this->once())
            ->method('save');

        $simulator = new Simulator($dataGeneratorMock, $dataBaseMock);

        $simulator->simulate();
    }

    public function testSimulateUpdate()
    {
        $dataGeneratorMock = $this->getMock('Jungheinrich\DataGenerator', ['getSimulatedData']);
        $dataBaseMock = $this->getMock('Jungheinrich\Database', ['update']);

        $dataGeneratorMock->expects($this->once())
            ->method('getSimulatedData')
            ->will($this->returnValue(['end' => 'test']));

        $dataBaseMock->expects($this->once())
            ->method('update');

        $simulator = new Simulator($dataGeneratorMock, $dataBaseMock);

        $simulator->simulate();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Could not generated data.
     */
    public function testException()
    {
        $dataGeneratorMock = $this->getMock('Jungheinrich\DataGenerator', ['getSimulatedData']);
        $dataBaseMock = $this->getMock('Jungheinrich\Database', ['update']);

        $dataGeneratorMock->expects($this->once())
            ->method('getSimulatedData')
            ->will($this->returnValue(false));

        $dataBaseMock->expects($this->never())
            ->method('update');

        $dataBaseMock->expects($this->never())
            ->method('save');

        $simulator = new Simulator($dataGeneratorMock, $dataBaseMock);

        $simulator->simulate();
    }
}
