<?php

namespace Tests\Unit;

use Jungheinrich\DataGenerator;

class DataGeneratorTest extends \PHPUnit_Framework_TestCase
{
    public function testGetSimulatedInsertData()
    {
        $dataGenerator = new DataGenerator();

        $actualValue = $dataGenerator->getSimulatedData(0);

        $this->assertCount(4, $actualValue);
        $this->assertArrayHasKey('forkliftId', $actualValue);
        $this->assertArrayHasKey('driverId', $actualValue);
        $this->assertArrayHasKey('start', $actualValue);
        $this->assertArrayHasKey('id', $actualValue);

        $this->assertSame(8, mb_strlen($actualValue['forkliftId']));
        $this->assertSame(14, mb_strlen($actualValue['driverId']));
        $this->assertTrue($actualValue['start'] !== false);
        $this->assertSame(1, $actualValue['id']);

        $reflProp = new \ReflectionProperty($dataGenerator, 'updateKeys');
        $reflProp->setAccessible(true);
        $actualValue = $reflProp->getValue($dataGenerator);

        $this->assertCount(1, $actualValue);
        $this->assertSame([1], $actualValue);

        $reflProp = new \ReflectionProperty($dataGenerator, 'autoIncrement');
        $reflProp->setAccessible(true);
        $actualValue = $reflProp->getValue($dataGenerator);

        $this->assertSame(2, $actualValue);

    }

    public function testGetSimulatedUpdateData()
    {
        $dataGenerator = new DataGenerator();

        $reflProp = new \ReflectionProperty($dataGenerator, 'updateKeys');
        $reflProp->setAccessible(true);
        $reflProp->setValue($dataGenerator, [1]);

        $actualValue = $dataGenerator->getSimulatedData(1);

        $this->assertCount(2, $actualValue);
        $this->assertArrayHasKey('end', $actualValue);
        $this->assertArrayHasKey('id', $actualValue);
        $this->assertTrue($actualValue['end'] !== false);
        $this->assertSame(1, $actualValue['id']);
    }

    public function testGetSimulatedUpdateDataReturnsFalse()
    {
        $dataGenerator = new DataGenerator();

        $actualValue = $dataGenerator->getSimulatedData(1);

        $this->assertCount(4, $actualValue);
        $this->assertArrayHasKey('forkliftId', $actualValue);
        $this->assertArrayHasKey('driverId', $actualValue);
        $this->assertArrayHasKey('start', $actualValue);
        $this->assertArrayHasKey('id', $actualValue);

        $this->assertSame(8, mb_strlen($actualValue['forkliftId']));
        $this->assertSame(14, mb_strlen($actualValue['driverId']));
        $this->assertTrue($actualValue['start'] !== false);
        $this->assertSame(1, $actualValue['id']);

        $reflProp = new \ReflectionProperty($dataGenerator, 'updateKeys');
        $reflProp->setAccessible(true);
        $actualValue = $reflProp->getValue($dataGenerator);

        $this->assertCount(1, $actualValue);
        $this->assertSame([1], $actualValue);

        $reflProp = new \ReflectionProperty($dataGenerator, 'autoIncrement');
        $reflProp->setAccessible(true);
        $actualValue = $reflProp->getValue($dataGenerator);

        $this->assertSame(2, $actualValue);

    }

    public function testGetRandomString()
    {
        $dataGenerator = new DataGenerator();

        $actualValue = $dataGenerator->getRandomString(1);

        $this->assertSame(1, strlen($actualValue));
        $this->assertTrue(ctype_alnum($actualValue));
    }
}
