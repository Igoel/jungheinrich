#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

$t = microtime(true);

if (count($argv) > 2) {
    die('To many arguments given. Only one argument supported.' . "\n");
}

if (!isset($argv[1])) {
    die('Argument one missing.' . "\n");
}

$iterations = (int)$argv[1];

if (!$iterations > 0) {
    die('Wrong type for argument one: Supported type is integer.' . "\n");
}

try {
    $simulator = new \Jungheinrich\Simulator(new \Jungheinrich\DataGenerator(), new \Jungheinrich\Database(), true);

    echo "\n";

    for ($i = 0; $i < $iterations; $i++) {
        progressBar($i + 1, $iterations);
        $simulator->simulate();
    }

    echo "\n    " . $argv[1] . ' simulations done.' . "\n";
    echo 'Done duration: ' . round(microtime(true) - $t, 5) . ' seconds' . "\n\n";

} catch (\Exception $e) {
    echo "\n" . 'Unexpected Error: ' . "\n";
    echo '      ' . $e->getMessage() . "\n\n";
}

// beautify progress
function progressBar($done, $total) {
    $perc = floor(($done / $total) * 50);
    $left = 50 - $perc;
    $write = sprintf("\033[0G\033[2K[%'={$perc}s>%-{$left}s] - $perc%% - $done/$total", "", "");
    fwrite(STDERR, $write);
}
