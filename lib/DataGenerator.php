<?php

namespace Jungheinrich;

class DataGenerator
{
    private $updateKeys = [];


    private $autoIncrement = 1;

    /**
     * Returns simulated usage messages.
     *
     * @param int $rand
     * @return array
     */
    public function getSimulatedData($rand)
    {
        $data = [];

        $randTimestamp = mt_rand(1000000000, 1000100000);

        if ($rand === 1) {
            $data = $this->getUpdateData($randTimestamp);
            if (!$data) {
                $rand = 0;
            }
        }

        if ($rand === 0) {
            $data = $this->getInsertData($randTimestamp);
        }

        return empty($data) ? false : $data;
    }

    /**
     * @param int $length
     * @return string
     */
    public function getRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    /**
     * @param int $randTimestamp
     * @return bool|array
     */
    private function getUpdateData($randTimestamp)
    {
        //Get an existing key, if no key exists, make an insert instead
        if (count($this->updateKeys) > 0) {
            $data['id'] = array_pop($this->updateKeys);
            $data['end'] = date('Y-m-d H:i:s', $randTimestamp + 600);
        } else {
            return false;
        }

        return $data;
    }

    /**
     * @param int $randTimestamp
     * @return array
     */
    private function getInsertData($randTimestamp)
    {
        $data['id'] = $this->autoIncrement;
        $data['forkliftId'] = $this->getRandomString(8);
        $data['driverId'] = $this->getRandomString(14);
        $data['start'] = date('Y-m-d H:i:s', $randTimestamp);

        //add key
        $this->updateKeys[] = $this->autoIncrement;
        //increment
        $this->autoIncrement++;

        return $data;
    }
}

