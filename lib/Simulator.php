<?php

namespace Jungheinrich;

class Simulator
{
    /**
     * @var DataGenerator
     */
    private $dataGenerator;

    /**
     * @var Database
     */
    private $database;

    /**
     * Simulator constructor.
     *
     * @param DataGenerator $dataGenerator
     * @param Database $database
     * @param bool $initializeTable
     */
    public function __construct(DataGenerator $dataGenerator, Database $database, $initializeTable = false)
    {
        $this->dataGenerator = $dataGenerator;
        $this->database = $database;

        if ($initializeTable) {
            $this->initializeSimulationTable();
        }
    }

    /**
     * Simulates n inserts + updates.
     *
     * @throws \Exception
     */
    public function simulate()
    {
        // 0 = insert, 1 = update
        $rand = mt_rand(0, 1);

        $data = $this->dataGenerator->getSimulatedData($rand);

        if (!$data) {
            throw new \Exception('Could not generated data.');
        }

        //check if end date is not set
        if (!isset($data['end'])) {
            $this->database->save($data);
        } else {
            $this->database->update($data);
        }
    }

    /**
     * Mini migration. Should normally be in an extra file.
     */
    private function initializeSimulationTable()
    {
        $this->database->createTable();
    }
}

