<?php

namespace Jungheinrich;

class Database
{
    /**
     * @var \PDO
     */
    public $db;

    public function __construct()
    {
        $this->db = new \PDO('sqlite::memory:');
    }

    /**
     * Insert a new forklift message.
     *
     * @param array $data
     * @throws \Exception
     */
    public function save($data)
    {
        if (array_key_exists('end', $data)) {
            unset($data['end']);
        }

        $stmt = $this->db->prepare("
                INSERT INTO `forklift_usage` (`id`, `forklift_id`, `driver_id`, `start`)
                VALUES (:id, :forkliftId, :driverId, :start);
            ");

        if (!$stmt) {
            throw new \Exception('Query Error: ' . json_encode($this->db->errorInfo()));
        }

        $result = $stmt->execute($data);

        if (!$result) {
            throw new \Exception('Query Error: ' . json_encode($this->db->errorInfo()));
        }
    }

    /**
     * Updates an existing forklift message.
     *
     * @param array $data
     * @throws \Exception
     */
    public function update($data)
    {
        // used all insert fields in where clause, because there is only no single unique field
        $stmt = $this->db->prepare("
                UPDATE `forklift_usage`
                  SET `end` = :end
                WHERE `id` = :id;
            ");

        if (!$stmt) {
            throw new \Exception('Query Error: ' . $this->db->errorInfo());
        }

        $result = $stmt->execute($data);

        if (!$result) {
            throw new \Exception('Query Error: ' . $this->db->errorInfo());
        }
    }

    public function createTable()
    {
        $this->db->exec("DROP TABLE IF EXISTS `forklift_usage`;");
        $result = $this->db->exec("
            CREATE TABLE `forklift_usage` (
              `id` INT NOT NULL,
              `forklift_id` VARCHAR(8) NOT NULL,
              `driver_id` VARCHAR(14) NULL,
              `start` DATETIME NOT NULL,
              `end` DATETIME,
              PRIMARY KEY (`id`)
              );
        ");

        if ($result === false) {
            throw new \Exception('Could not migrate.');
        }
    }
}

