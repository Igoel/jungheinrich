# Simulator #

### What is this repository for? ###

This application simulates data imports of generated forklift usage messages.
It`s only a simplified version, so there is no REST-endpoint or so. Also in memory storage is used, so that the application is running out of the box, without many dependencies.
In addition an extra unique id field was added to the table to simplify updates.

To run 100 simulations: 
php simulate.php 100

Note: 100000 simulations took around 8-9 seconds on my machine.

Version: 0.0.1

###Further suggestions###
This applications is only rudimentary: data generation and saving/updating.

To extend this simulator it would be possible to send data via a REST endpoint or so.

Validate the incoming data. Maybe then save the data in a queue (like RabbitMQ, Redis ...) and

write a consumer to import the messages into a database (like, MariaDB, MongoDB,...) and not into memory.

So that in the end full integration test are possible.

### How do I get set up? ###

**Dependencies**

 * PHP 5.6
 * Composer
 * pdo & sqlite (default activated in php)

**Set up**

 * run composer install in the root dir

**Database**

 * uses Sqlite in memory storage

**How to run tests**

 * in root dir run: phpunit --configuration=phpunit.xml